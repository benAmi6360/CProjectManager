.SUFFIXES:

CC = gcc
LANG = c
SRC = ./src
BUILD = ./build
MAIN_EXEC = cprojmgr
CFLAGS = -Wall -Iheaders -I${BUILD}/lib/headers -g -p
LIBS = $(wildcard ${BUILD}/lib/objs/*.o)

# Get a list of all subdirectories and their subdirectories recursively
SUBDIRS = $(shell find $(SRC) -type d)

# Get a list of all C source files recursively in all subdirectories
FILES = $(wildcard $(addsuffix /*.${LANG},$(SUBDIRS)))
OBJS = $(patsubst ${SRC}/%.c, ${BUILD}/objs/%.o, ${FILES})
ALL_OBJS = ${LIBS} ${OBJS}

all: prepare_build clear_console ${MAIN_EXEC}

prepare_build:
	@mkdir -p ${BUILD}/objs
	@mkdir -p ${BUILD}/libs/objs
	@mkdir -p ${BUILD}/libs/headers


clear_console:
	echo "${OBJS}"
	clear
 
${MAIN_EXEC}: ${ALL_OBJS}
	${CC} ${ALL_OBJS} -o $@

${BUILD}/objs/%.o: ${SRC}/%.c
	${CC} ${CFLAGS} -c $< -o $@

clean:
	rm -rf ${OBJS} ${MAIN_EXEC}

.PHONY: all clean