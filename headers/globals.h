#ifndef GLOBALS_H
#define GLOBALS_H


const char makefile_template[] = 
						".SUFFIXES:\n"
                        "CC = %s\n"
                        "SRC = ./src\n"
						"LANG = %s\n"
                        "MAIN_EXEC = %s\n"
                        "BUILD = ./build\n"
                        "CFLAGS = -Wall -Iheaders -I${BUILD}/lib/headers -g\n"
                        "LIBS = $(wildcard ${BUILD}/lib/objs/*.o)\n"
						"SUBDIRS = $(shell find $(SRC) -type d)\n"
                        "FILES = $(wildcard $(addsuffix /*.${LANG},$(SUBDIRS)))\n"
                        "OBJS = $(patsubst ${SRC}/%%.${LANG}, ${BUILD}/objs/%%.o, ${FILES})\n"
                        "ALL_OBJS = ${LIBS} ${OBJS}\n"
                        "\n"
                        "all: prepare_build clear_console ${MAIN_EXEC}\n"
						"\n"
						"prepare_build:\n"
						"\t@mkdir -p ${BUILD}/objs\n"
						"\t@mkdir -p ${BUILD}/libs/objs\n"
						"\t@mkdir -p ${BUILD}/libs/headers\n"
                        "\n"
                        "clear_console:\n"
                        "\tclear\n"
                        "\n"
                        "${MAIN_EXEC}: ${ALL_OBJS}\n"
                        "\t${CC} ${ALL_OBJS} -o $@\n"
                        "\n"
                        "${BUILD}/objs/%%.o: ${SRC}/%%.${LANG}\n"
                        "\t${CC} ${CFLAGS} -c $< -o $@\n"
                        "\n"
                        "clean:\n"
                        "\trm -rf ${OBJS} ${MAIN_EXEC}\n"
                        "\n"
                        ".PHONY: all clean\n";

const char *required_files[] = { 
    ".gitignore",
    "readme.md",
    "Makefile",
	"src/main.c",
	".cpi/requirements.txt"
};


const int req_files_len = 5;

const char *git_ignore_content = 
	"build/"
    "%s\n";

const char *readme_content = 
    "# %s\n";

const char *main_c_content = 	
	"#include <stdio.h>\n\n"
	"int main(int argc, char **argv) {\n"
	"\tprintf(\"Hello, World!\\n\");\n"
	"}\n";


const char *main_cpp_content = 	
	"#include <iostream>\n\n"
	"int main(int argc, char **argv) {\n"
	"\tstd::cout << \"Hello, World!\\n\";\n"
	"}\n";



const char *required_dirs[] = {
    "build/",
    "build/lib/",
    "build/lib/objs/",
    "build/lib/headers/",
    "build/objs/",
    "headers/",
    "src/",
	".cpi/"
};


const int req_dirs_len = 8;

const char *rm_rf = "rm -rf %s*";

const int rm_length = 9;

#endif