# C Project Manager

> A cli tool to help me create better structured C projects

if you want to build it just `make` it

## Todo
- Switch `Makefile` to `build.sh` (This might or might not happen just thought of a way around it)
- Make this in sync with other projects I have in mind

## Example
`./cprojmgr {Project Name} {Language}` will create a project directory with the language of choice<br>
Languages can only be c or cpp <br>

The command `./cprojmgr MyProject c` will result in this <br>
```
/MyProject
    (
    /.cgi
        requirements.txt # let me cook
    )
    /build
        /lib
            /objs
            /headers
        /objs
    /headers
    /src
        main.c
    .gitignore
    Makefile
    readme.md
```

## Default Content

 - By default `.gitignore` holds the object files folder and the main executable
 - The `readme.md` contains a simple `# Project Name` text
 - The Makefile holds build script to build all source files in `src`
 - Will create a main file printing "Hello, World!" in the chosen language
 - Will initialize git inside the project folder
