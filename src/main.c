#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "setup.h"

int main(int argc, char **argv) 
{ 
    if (argc != 3)
    {
        fprintf(stderr, "Wrong number of arguments: %d\n", argc);
        exit(1);
    }
    if (strcmp(argv[2], "c") != 0 && strcmp(argv[2], "cpp") != 0)
    {
        fprintf(stderr, "Bad language (not C or CPP)\nLang: %s\n", argv[2]);
        return -1;
    }
    setup_proj(argv[1], argv[2]);
    return 0;
} 