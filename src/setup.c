#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>

#include "globals.h"

#ifndef PATH_MAX
#define PATH_MAX 4096
#endif

FILE *f;

int create_dir_if_not_exists(const char *path)
{
	struct stat st;
    int res = stat(path, &st);
    if (res != 0 || !S_ISDIR(st.st_mode))
    {
        if ((res = mkdir(path, S_IRWXU)) == 0) {
            return 0;
        }
        return -1;
    }
	char buffer[strlen(path) + rm_length];
	snprintf(buffer, sizeof(buffer), rm_rf, path);
	system(buffer);
    return 0;
}


void create_dirs(char *path_to_proj)
{   
    for (int i = 0; i < req_dirs_len; i++)
    {
        const char *curr_dir = required_dirs[i];
        create_dir_if_not_exists(curr_dir);
    }
}

void create_files(char *path_to_proj)
{
    for (int i = 0; i < req_files_len; i++)
    {
        const char *curr_file = required_files[i];
        f = fopen(curr_file, "w");
        fclose(f);
    }
}

void change_dir(const char *path)
{
    if (chdir(path) != 0) {
        perror("chdir");
        exit(1);
    }
}

void write_to_gitignore(char *path)
{
    f = fopen(".gitignore", "w");
    fprintf(f, git_ignore_content, path);
    fclose(f);
}

void write_to_makefile(char *path, char *lang)
{
    f = fopen("Makefile", "w");
	char compiler[4];
	if (strncmp(lang, "cpp", 3) == 0)
	{
		strcpy(compiler, "g++");
	}
	else {
		strcpy(compiler, "gcc");
	}
    fprintf(f, makefile_template, compiler, lang, path);
    fclose(f);
}

void write_to_readme(char *path)
{
    f = fopen("readme.md", "w");
    fprintf(f, readme_content, path);
    fclose(f);
}

void write_to_main(char *lang)
{
	char buffer[20];
	snprintf(buffer, sizeof(buffer), "src/main.%s", lang);
	f = fopen(buffer, "w");
	if (strcmp(lang, "cpp") == 0)
	{
		fprintf(f, main_cpp_content);
	}
	else {
		fprintf(f, main_c_content);
	}
	fclose(f);
}

void setup_proj(char *proj_name, char *lang)
{
    create_dir_if_not_exists(proj_name);
    change_dir(proj_name);
    create_dirs(proj_name);
    create_files(proj_name);
    write_to_gitignore(proj_name);
    write_to_makefile(proj_name, lang);
    write_to_readme(proj_name);
	write_to_main(lang);
	system("git init");
}  